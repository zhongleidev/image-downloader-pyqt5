class ImageBean:
    def __init__(self):
        self.album = ""
        self.imgurl = ""
        self.imgname = ""
        self.imgwidth = ""
        self.imgheight = ""

    def getAlbum(self):
        return self.album
    def setAlbum(self,album_str):
        self.album = album_str
    def getImgUrl(self):
        return self.imgurl
    def setImgUrl(self,url_str):
        self.imgurl = url_str
    def getImgName(self):
        return self.imgname
    def setImgName(self, name_str):
        self.imgname = name_str
    def getImgWidth(self):
        return self.imgwidth
    def setImgWidth(self, width_str):
        self.imgwidth = width_str
    def getImgHeight(self):
        return self.imgheight
    def setImgHeight(self, height_str):
        self.imgheight = height_str
    
    def __repr__(self):
        return repr((self.album, self.imgurl, self.imgname, self.imgwidth, self.imgheight))